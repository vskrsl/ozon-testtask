import Vue from 'vue';
import Vuex from 'vuex';
import { parse, isSameDay, isAfter } from 'date-fns';

// import calendarStore from './modules/calendar';

// interface IEvents {
//   date: Date;
//   time: Date;
//   title: string;
// }

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    // TODO: не нравится что я сразу инициализирую стейт чтобы не было null
    // подумать в каком lifecycle hooks делать initState()
    calendarMonth: new Date(),
    selectedDate: new Date(),
    // NOTE: предположим что в будущем будем хранить event по timestamp и
    // получать его откуда-то с сервера/базы -> после получения превращать в
    // нужный нам формат и уже в таком формате хранить в state
    events: [
      {
        date: parse('26.02.2020 12:00', 'dd.MM.yyyy HH:mm', new Date()),
        time: parse('26.02.2020 12:00', 'dd.MM.yyyy HH:mm', new Date()),
        title: 'to do smth 1',
      },
      {
        date: parse('26.02.2020 13:00', 'dd.MM.yyyy HH:mm', new Date()),
        time: parse('26.02.2020 13:00', 'dd.MM.yyyy HH:mm', new Date()),
        title: 'to do smth 2',
      },
      {
        date: parse('26.02.2020 12:00', 'dd.MM.yyyy HH:mm', new Date()),
        time: parse('26.02.2020 12:00', 'dd.MM.yyyy HH:mm', new Date()),
        title: 'to do smth 1',
      },
      {
        date: parse('26.02.2020 13:00', 'dd.MM.yyyy HH:mm', new Date()),
        time: parse('26.02.2020 13:00', 'dd.MM.yyyy HH:mm', new Date()),
        title: 'to do smth 2',
      },
      {
        date: parse('26.02.2020 12:00', 'dd.MM.yyyy HH:mm', new Date()),
        time: parse('26.02.2020 12:00', 'dd.MM.yyyy HH:mm', new Date()),
        title: 'to do smth 1',
      },
      {
        date: parse('26.02.2020 13:00', 'dd.MM.yyyy HH:mm', new Date()),
        time: parse('26.02.2020 13:00', 'dd.MM.yyyy HH:mm', new Date()),
        title: 'to do smth 2',
      },
      {
        date: parse('26.02.2020 12:00', 'dd.MM.yyyy HH:mm', new Date()),
        time: parse('26.02.2020 12:00', 'dd.MM.yyyy HH:mm', new Date()),
        title: 'to do smth 1',
      },
      {
        date: parse('26.02.2020 13:00', 'dd.MM.yyyy HH:mm', new Date()),
        time: parse('26.02.2020 13:00', 'dd.MM.yyyy HH:mm', new Date()),
        title: 'to do smth 2',
      },
      {
        date: parse('26.02.2020 12:00', 'dd.MM.yyyy HH:mm', new Date()),
        time: parse('26.02.2020 12:00', 'dd.MM.yyyy HH:mm', new Date()),
        title: 'to do smth 1',
      },
      {
        date: parse('26.02.2020 13:00', 'dd.MM.yyyy HH:mm', new Date()),
        time: parse('26.02.2020 13:00', 'dd.MM.yyyy HH:mm', new Date()),
        title: 'to do smth 2',
      },
      {
        date: parse('26.02.2020 12:00', 'dd.MM.yyyy HH:mm', new Date()),
        time: parse('26.02.2020 12:00', 'dd.MM.yyyy HH:mm', new Date()),
        title: 'to do smth 1',
      },
      {
        date: parse('26.02.2020 13:00', 'dd.MM.yyyy HH:mm', new Date()),
        time: parse('26.02.2020 13:00', 'dd.MM.yyyy HH:mm', new Date()),
        title: 'to do smth 2',
      },
      {
        date: parse('26.02.2020 12:00', 'dd.MM.yyyy HH:mm', new Date()),
        time: parse('26.02.2020 12:00', 'dd.MM.yyyy HH:mm', new Date()),
        title: 'to do smth 1',
      },
      {
        date: parse('26.02.2020 13:00', 'dd.MM.yyyy HH:mm', new Date()),
        time: parse('26.02.2020 13:00', 'dd.MM.yyyy HH:mm', new Date()),
        title: 'to do smth 2',
      },
      {
        date: parse('29.02.2020 14:00', 'dd.MM.yyyy HH:mm', new Date()),
        time: parse('29.02.2020 14:00', 'dd.MM.yyyy HH:mm', new Date()),
        title: 'Позвонить Геннадию.',
      },
      {
        date: parse('29.02.2020 12:00', 'dd.MM.yyyy HH:mm', new Date()),
        time: parse('29.02.2020 12:00', 'dd.MM.yyyy HH:mm', new Date()),
        title: 'Купить хлеба.',
      },
      {
        date: parse('29.02.2020 13:00', 'dd.MM.yyyy HH:mm', new Date()),
        time: parse('29.02.2020 13:00', 'dd.MM.yyyy HH:mm', new Date()),
        title: 'Не тупить целый день на лепре.',
      },
      {
        date: parse('29.01.2020 12:00', 'dd.MM.yyyy HH:mm', new Date()),
        time: parse('29.01.2020 12:00', 'dd.MM.yyyy HH:mm', new Date()),
        title: 'Позвонить Геннадию.',
      },
    ],
  },
  mutations: {
    setSelectedDate(state, payload: Date) {
      state.selectedDate = payload;
    },
  },
  actions: {
    initState(ctx): void {
      ctx.commit('initState');
    },
    setSelectedDate(ctx, payload: Date): void {
      ctx.commit('setSelectedDate', payload);
    },
  },
  getters: {
    getMonth: state => state.calendarMonth,
    getSelectedDay: state => state.selectedDate,
    getSelectedDayEvents: state =>
      state.events
        .filter(e => isSameDay(e.date, state.selectedDate))
        .sort((a, b) => (isAfter(a.time, b.time) ? 1 : -1)),
    getEventsDates: state => {
      console.log('getEventsDates fire');
      return state.events.map(e => e.date);
    },
  },
});
