import { Component, Prop } from 'vue-property-decorator';
import { VueComponent } from '../../../shims-vue';

import styles from './CalendarHeader.css?module';

interface Props {
  content: string;
}

@Component
export default class CalendarHeader extends VueComponent<Props> {
  @Prop() private content!: string;
  render() {
    return <div class={styles.calendar__header}>{this.content}</div>;
  }
}
