import { Component } from 'vue-property-decorator';
import { VueComponent } from '../../../shims-vue';
import { isSameDay, isSameMonth } from 'date-fns';
import { calendarMatrix } from '../calendarUtils';
import CalendarCell from '../calendar-cell/CalendarCell';

import styles from './CalendarDays.css?module';

@Component
export default class CalendarDays extends VueComponent {
  calendarMatrix: Date[][] = calendarMatrix(this.month);

  get markedDays(): Date[] {
    return this.$store.getters.getEventsDates;
  }
  get selectedDay(): Date {
    return this.$store.getters.getSelectedDay;
  }
  get month(): Date {
    return this.$store.getters.getMonth;
  }

  isMarked(day: Date): boolean {
    return this.markedDays.some(e => isSameDay(e, day as Date));
  }

  isSelected(day: Date): boolean {
    return isSameDay(day as Date, this.$store.getters.getSelectedDay);
  }
  isSameMonth(day: Date): boolean {
    return isSameMonth(day as Date, this.$store.getters.getMonth);
  }

  render() {
    return (
      <div class={styles.calendar__days}>
        {!this.calendarMatrix
          ? 'Что-то пошло не так'
          : this.calendarMatrix.map(week => (
              <div class={styles.calendar__daysrow}>
                {week.map(day => (
                  <CalendarCell
                    type="day"
                    payload={day}
                    isMarked={this.isMarked(day)}
                    isSelected={this.isSelected(day)}
                    isSameMonth={this.isSameMonth(day)}
                  />
                ))}
              </div>
            ))}
      </div>
    );
  }
}
