// date-fns
import {
  format,
  startOfMonth,
  endOfMonth,
  eachDayOfInterval,
  eachWeekOfInterval,
  startOfISOWeek,
  endOfISOWeek,
  isSameMonth,
  // addMonths - для простейших тестов
  addMonths,
} from 'date-fns';

// Возвращает конкатенированную строку переданных классов.
export function classnames(...args: string[]): string {
  return args.join(' ');
}

/**
 * Формирует массив календаря.
 * [['','',Date,Date,Date,Date,Date],
 * ['','',Date,Date,Date,Date,Date],
 * ...
 * [Date,Date,Date,Date,'','','']]
 *
 * @param currentDay - день от которого считается календарь на месяц
 */
export function calendarMatrix(currentDay: Date): Date[][] {
  const today = currentDay;

  // если нам всегда нужна 6 неделя то:
  // - будем проверять длинну weeksOfMonth
  // - добавлять одну неделю под дате endOfMonth(today) + 1 week
  // - заполнять daysOfWeek
  const weeksOfMonth = eachWeekOfInterval(
    { start: startOfMonth(today), end: endOfMonth(today) },
    { weekStartsOn: 1 },
  );

  const daysOfWeeks = weeksOfMonth.map(week =>
    eachDayOfInterval({ start: startOfISOWeek(week), end: endOfISOWeek(week) }),
  );

  // const calendar = daysOfWeeks.map(week => week.map(day => (isSameMonth(today, day) ? day : null)));
  const calendar = daysOfWeeks.map(week => week.map(day => day));

  return calendar;
}
