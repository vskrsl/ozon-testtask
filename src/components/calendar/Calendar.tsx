import { Component } from 'vue-property-decorator';
import { VueComponent } from '../../shims-vue';

import { format } from 'date-fns';
import { ru } from 'date-fns/locale';

import CalendarHeader from './calendar-header/CalendarHeader';
import CalendarWeekDays from './calendar-weekdays/CalendarWeekDays';
import CalendarDays from './calendar-days/CalendarDays';
import CalendarDetails from './calendar-details/CalendarDetails';

import './Calendar.css';

@Component
export default class Calendar extends VueComponent {
  get month() {
    return this.$store.getters.getMonth;
  }

  render() {
    return (
      <div class="calendar theme theme_color_default theme_font_default theme_size_default theme_space_default">
        <div class="calendar__body">
          <CalendarHeader content={format(this.month, 'LLLL', { locale: ru })} />
          <CalendarWeekDays />
          <CalendarDays />
        </div>
        <div class="calendar__details">
          <CalendarHeader content="События" />
          <CalendarDetails />
        </div>
      </div>
    );
  }
}
