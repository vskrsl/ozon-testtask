// NOTE:
// Хочется хранить разметку разных видов ячеек в этом компаненте, но:
// - пока не получилось реализовать scoped slots корректно.
// - через `slot='name'` получается что разметка передается из родителя (
// - через `if (props.type) ...` кажется некрасивым

import { Component, Prop } from 'vue-property-decorator';
import { VueComponent } from '../../../shims-vue';

import { format } from 'date-fns';

import './CalendarCell.css';

interface Props {
  type: 'weekday' | 'day';
  payload: string | Date | null;
  isMarked?: boolean | undefined;
  isSelected?: boolean | undefined;
  isSameMonth?: boolean | undefined;
}
@Component
export default class CalendarCell extends VueComponent<Props> {
  @Prop() private type!: 'weekday' | 'day';
  @Prop() private payload!: string | Date;
  @Prop() private isMarked!: boolean;
  @Prop() private isSelected!: boolean;
  @Prop() private isSameMonth!: boolean;

  // накидывает стили для ячейки в зависимсоти от props.
  // isMarked - красный
  // isSelected - кружок
  // !isSSameMonth - серый
  cellClasses(): string {
    let classes = ['calendar__cell'];

    if (this.isMarked) classes.push('calendar__cell_color_accent');
    if (this.isSelected) classes.push('calendar__cell_active');
    if (!this.isSameMonth) classes.push('calendar__cell_color_ghost');

    return classes.join(' ');
  }

  onButtonClick() {
    this.$store.dispatch('setSelectedDate', this.payload);
  }

  render() {
    switch (this.type) {
      case 'weekday':
        return <div class="calendar__cell calendar__cell_color_ghost">{this.payload}</div>;
      case 'day':
        return (
          <div class={this.cellClasses()}>
            <button onClick={this.onButtonClick}>
              <span>{format(this.payload as Date, 'd')}</span>
            </button>
          </div>
        );
      default:
        return <span>err</span>;
    }
  }
}
