import { Component, Prop } from 'vue-property-decorator';
import { VueComponent } from '../../../shims-vue';

import { format } from 'date-fns';
import { ru } from 'date-fns/locale';

interface Props {}

import './CalendarDetails.css';

interface IEvents {
  date: Date;
  time: Date;
  title: string;
}

@Component
export default class CalendarDetails extends VueComponent<Props> {
  get events() {
    return this.$store.getters.getSelectedDayEvents;
  }

  render() {
    return (
      <div class="list">
        <div class="scroll">
          {/* TODO: завернуть это в EventList -> EventItem */}
          {this.events.map((event: IEvents, idx: number) => {
            return (
              <div class="list__item" key={idx}>
                <div class="list__time">{format(event.time, 'HH:mm')}</div>
                <div class="list__title">{event.title}</div>
              </div>
            );
          })}
        </div>
      </div>
    );
  }
}
