import { Component, Prop } from 'vue-property-decorator';
import { VueComponent } from '../../../shims-vue';

import CalendarCell from '../calendar-cell/CalendarCell';

import styles from './CalendarWeekDays.css?module';

@Component
export default class CalendarWeekDays extends VueComponent {
  render() {
    return (
      <div class={styles.calendar__weekdays}>
        {['Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Вс'].map(weekday => (
          <CalendarCell type="weekday" payload={weekday} />
        ))}
      </div>
    );
  }
}
