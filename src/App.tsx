import { Component, Vue } from 'vue-property-decorator';
import Calendar from './components/calendar/Calendar';

import { addDays } from 'date-fns';

// темизация лежит в App.css
import './App.css';

@Component
export default class App extends Vue {
  // какие дни отмечать красным
  // markedDays: 'holidays' | Date[] = [addDays(new Date(), -10), addDays(new Date(), 3)];

  mounted() {
    // this.$store.commit('initState');
    // console.log(this.$store.getters.getEventsDates.toString());
  }

  // get markedDays(): Date[] {
  //   return this.$store.getters.getEventsDates;
  // }
  render() {
    return (
      <div id="app" class="theme container test">
        <Calendar />
      </div>
    );
  }
}
